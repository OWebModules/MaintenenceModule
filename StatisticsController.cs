﻿using MaintainenceModule.Models;
using MaintainenceModule.Services;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule
{
    public class StatisticsController : AppController
    {
        public async Task<ResultItem<OntologyStatistic>> GenerateStatistics(IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<OntologyStatistic>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                var elasticAgent = new StatisticsServiceAgentElastic(Globals);

                messageOutput?.OutputInfo("Generate Count Statistics...");

                var agentResult = await elasticAgent.GenerateOntologyStatistics(messageOutput);

                result.ResultState = agentResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                result.Result = new OntologyStatistic(agentResult.Result);

                var validationStatistics = await GenerateValidationStatistics(result.Result, messageOutput);

                messageOutput?.OutputInfo("Generated Count Statistics...");

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<OntologyStatistic>> GenerateValidationStatistics(OntologyStatistic statistic, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<OntologyStatistic>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = statistic
                };

                var elasticAgent = new StatisticsServiceAgentElastic(Globals);

                messageOutput?.OutputInfo("Generate Count Statistics...");

                var agentResult = await elasticAgent.GenerateValidationStatistics(statistic.Statistic, messageOutput);

                result.ResultState = agentResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }



                messageOutput?.OutputInfo("Generated Count Statistics...");

                return result;
            });

            return taskResult;
        }

        public StatisticsController(Globals globals) : base(globals)
        {
        }
    }
}
