﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class CleanupMultipleItemsByNameRequest
    {
        public string IdClassSource { get; set; }
        public string IdClassDest { get; set; }
        public List<string> ExcludeObjectIds { get; set; } = new List<string>();
        public List<string> IncludeObjectIds { get; set; } = new List<string>();
        public bool MergeAttributes { get; set; }
        public bool MergeRelations { get; set; }

        public CleanupMultipleItemsByNameRequest(string idClassSource, string idClassDest)
        {
            IdClassSource = idClassSource;
            IdClassDest = idClassDest;
        }
    }
}
