﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class DeleteAbandonedLeafsRequest
    {
        public string IdClass { get; private set; }
        public string IdRelationType { get; private set; }
        public string IdDirection { get; private set; }

        public DeleteAbandonedLeafsRequest(string idClass, string idRelationType, string idDirection)
        {
            IdClass = idClass;
            IdRelationType = idRelationType;
            IdDirection = idDirection;
        }
    }
}
