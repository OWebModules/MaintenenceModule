﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class CleanupMultipleGUIDsResult
    {
        public long CountMultipleFound { get; set; }

        public List<clsOntologyItem> MultipleGUIDs { get; set; } = new List<clsOntologyItem>();
    }
}
