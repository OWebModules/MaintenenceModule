﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class RelateObjectsWithObjectsByNameToAttributeResult
    {
        public long CountRelated { get; set; }

        public long CountSourceObjects { get; set; }
    }
}
