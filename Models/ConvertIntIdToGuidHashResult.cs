﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class ConvertIntIdToGuidHashResult
    {
        public long QueryCount { get; set; }
        public long DelCountObjects { get; set; }
        public long DelCountObjectRelations { get; set; }
        public long DelCountObjectAttributes { get; set; }

        public long CreateCountObjects { get; set; }

        public long CreateCountObjectRelations { get; set; }
        public long CreateCountObjectAttributes { get; set; }
    }
}
