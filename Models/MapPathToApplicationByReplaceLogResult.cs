﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class MapPathToApplicationByReplaceLogResult
    {
        public List<clsObjectRel> ApplicationToPathMap { get; set; } = new List<clsObjectRel>();
    }

    
}
