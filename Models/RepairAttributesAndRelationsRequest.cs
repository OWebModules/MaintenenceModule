﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class RepairAttributesAndRelationsRequest
    {
        public string IdClassToRepairObjects { get; private set; }

        public CancellationToken CancellationToken { get; private set; }

        public IMessageOutput MessageOutput { get; set; }


        public RepairAttributesAndRelationsRequest(string idClassToRepairObjects, CancellationToken cancellation)
        {
            IdClassToRepairObjects = idClassToRepairObjects;
            CancellationToken = CancellationToken;
        }
    }
}
