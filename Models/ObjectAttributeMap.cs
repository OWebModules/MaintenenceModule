﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class ObjectAttributeMap
    {
        public clsOntologyItem ObjectItem { get; set; }
        public clsObjectAtt Attribute { get; set; }
    }
}
