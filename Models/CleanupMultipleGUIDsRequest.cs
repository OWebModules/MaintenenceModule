﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class CleanupMultipleGUIDsRequest
    {
        public string IdClass { get; set; }
        public bool Check { get; set; } = true;
    }
}
