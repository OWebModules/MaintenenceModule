﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class DeleteUnrelatedObjectsResult
    {
        public List<clsOntologyItem> DeletedObjects { get; set; } = new List<clsOntologyItem>();
    }
}
