﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class RepairAttributesAndRelationsResult
    {
        public long CountObjects { get; set; }
        public long CountAttributes { get; set; }

        public long CountRelations { get; set; }
        public long CountAttributesChanged { get; set; }
        public long CountRelationsChanged { get; set; }
    }
}
