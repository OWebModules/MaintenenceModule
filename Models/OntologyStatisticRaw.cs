﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class OntologyStatisticRaw
    {
        public clsOntologyItem Statistic { get; set; }
        public clsObjectAtt CountAttributeTypesItem { get; set; }
        public clsObjectAtt CountAxiomViolationsItem { get; set; }
        public clsObjectAtt CountClassAttributesItem { get; set; }
        public clsObjectAtt CountClassesItem { get; set; }
        public clsObjectAtt CountClassRelationsItem { get; set; }
        public clsObjectAtt CountObjectAttributesItem { get; set; }
        public clsObjectAtt CountObjectRelationsItem { get; set; }
        public clsObjectAtt CountObjectsItem { get; set; }
        public clsObjectAtt CountRelationTypesItem { get; set; }

        public clsObjectAtt DateTimeStampItem { get; set; }

    }
}
