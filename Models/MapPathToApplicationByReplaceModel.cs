﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class MapPathToApplicationByReplaceModel
    {
        public List<clsObjectRel> ReplaceLogsToApplications { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> ReplaceLogsPaths { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> ReplaceLogsReplaced { get; set; } = new List<clsObjectAtt>();

    }
}
