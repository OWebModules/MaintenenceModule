﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class DeleteUnrelatedObjectsRequest
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public DeleteUnrelatedObjectsRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
