﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class RelationsWithoutRelationTypesResult
    {
        public List<clsClassRel> ClassRelations { get; set; } = new List<clsClassRel>();
        public List<clsObjectRel> ObjectRelations { get; set; } = new List<clsObjectRel>();
    }
}
