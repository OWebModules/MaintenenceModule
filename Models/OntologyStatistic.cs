﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class OntologyStatistic:OntologyStatisticRaw
    {
        public DateTime? CreateStamp
        {
            get
            {
                return DateTimeStampItem?.Val_Datetime;
            }
        }

        public long CountAttributeTypes { get { return CountAttributeTypesItem?.Val_Lng ?? 0; } }
        public long CountAxiomViolations { get { return CountAxiomViolationsItem?.Val_Lng ?? 0; } }
        public long CountClassAttributes { get { return CountClassAttributesItem?.Val_Lng ?? 0; } }
        public long CountClasses { get { return CountClassesItem?.Val_Lng ?? 0; } }
        public long CountClassRelations { get { return CountClassRelationsItem?.Val_Lng ?? 0; } }
        public long CountObjectRelations { get { return CountObjectRelationsItem?.Val_Lng ?? 0; } }
        public long CountObjects { get { return CountObjectsItem?.Val_Lng ?? 0; } }
        public long CountRelationTypes { get { return CountRelationTypesItem?.Val_Lng ?? 0; } }

        public OntologyStatistic(OntologyStatisticRaw rawItem)
        {
            CountAttributeTypesItem = rawItem.CountAttributeTypesItem;
            CountAxiomViolationsItem = rawItem.CountAxiomViolationsItem;
            CountClassAttributesItem = rawItem.CountClassAttributesItem;
            CountClassesItem = rawItem.CountClassesItem;
            CountClassRelationsItem = rawItem.CountClassRelationsItem;
            CountObjectAttributesItem = rawItem.CountObjectAttributesItem;
            CountObjectRelationsItem = rawItem.CountObjectRelationsItem;
            CountObjectsItem = rawItem.CountObjectsItem;
            CountRelationTypesItem = rawItem.CountRelationTypesItem;
            DateTimeStampItem = rawItem.DateTimeStampItem;
        }
    }

    public class AxiomValidation : AxiomValidationRaw
    {
        
    }
}
