﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class DeleteAbandonedLeafsResult
    {
        public long CountDeletedLeafs { get; set; }
    }
}
