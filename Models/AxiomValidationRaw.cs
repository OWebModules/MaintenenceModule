﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class AxiomValidationRaw
    {
        public clsOntologyItem Statistic { get; set; }
        public clsOntologyItem AxiomValidation { get; set; }
        public clsObjectAtt Count { get; set; }
        public clsObjectAtt Violations { get; set; }

        public List<clsObjectRel> References { get; set; }
    }
}
