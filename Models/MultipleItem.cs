﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class MultipleItem
    {
        public clsOntologyItem DestItem { get; set; }
        public List<clsOntologyItem> SourceItems { get; set; } = new List<clsOntologyItem>();
    }
}
