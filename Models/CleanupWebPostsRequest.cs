﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class CleanupWebPostsRequest
    {
        public IMessageOutput MessageOutput { get; set; }
    }
}
