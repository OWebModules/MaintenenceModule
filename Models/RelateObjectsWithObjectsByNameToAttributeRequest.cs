﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Models
{
    public class RelateObjectsWithObjectsByNameToAttributeRequest
    {
        public string IdClassSource { get; private set; }
        public string IdClassDest { get; private set; }
        public string IdAttributeTypeDest { get; private set; }

        public clsOntologyItem RelationType { get; private set; }
        public clsOntologyItem Direction { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public RelateObjectsWithObjectsByNameToAttributeRequest(string idClassSource, string idClassDest, string idAttributeTypeDest, clsOntologyItem relationType, clsOntologyItem direction )
        {
            IdClassSource = idClassSource;
            IdClassDest = idClassDest;
            IdAttributeTypeDest = idAttributeTypeDest;
            RelationType = relationType;
            Direction = direction;
        }
    }
}
