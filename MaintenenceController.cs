﻿using ElasticSearchNestConnector;
using MaintainenceModule.Models;
using MaintainenceModule.Services;
using MaintainenceModule.Validation;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using OntoMsg_Module.Converters;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule
{
    public class MaintenenceController : AppController
    {
        public async Task<ResultItem<DeleteUnrelatedObjectsResult>> DeleteUnrelatedObjects(DeleteUnrelatedObjectsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<DeleteUnrelatedObjectsResult>>(async() =>
            {
                var result = new ResultItem<DeleteUnrelatedObjectsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new DeleteUnrelatedObjectsResult()
                };

                result.ResultState = Validation.ValidationController.ValidateDeleteUnrelatedObjectsRequest(request, Globals);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var elasticAgent = new ServiceAgentElastic(Globals);

                request.MessageOutput?.OutputInfo("Getting config...");
                var configResult = await elasticAgent.GetDeleteUnrelatedObjectsConfig(request);
                result.ResultState = configResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Having config.");

                request.MessageOutput?.OutputInfo("Getting classes...");
                var getClassesResult = await elasticAgent.GetClasses(configResult.Result);
                result.ResultState = getClassesResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Having {getClassesResult.Result.Count} classes.");

                foreach (var classItem in getClassesResult.Result)
                {
                    var searchObjects = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID_Parent = classItem.GUID
                        }
                    };

                    var getObjectsResult = await elasticAgent.GetObjectsOfClass(classItem);
                    result.ResultState = getObjectsResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var count = getObjectsResult.Result.Count;
                    var pos = 0;
                    var range = 500;

                    while (pos < count)
                    {
                        if (count - pos < range)
                        {
                            range = count - pos;
                        }
                        var rangeObjects = getObjectsResult.Result.GetRange(pos, range).ToList();

                        var getRelationsResult = await elasticAgent.GetRelationsOfObjects(rangeObjects, pos, range);

                        result.ResultState = getClassesResult.ResultState;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }

                        var objectsToDelete = (from obj in rangeObjects
                                               join relLeftRight in getRelationsResult.Result on obj.GUID equals relLeftRight.ID_Object into relLeftRights
                                               from relLeftRight in relLeftRights.DefaultIfEmpty()
                                               join relRightLeft in getRelationsResult.Result on obj.GUID equals relRightLeft.ID_Other into relRightLefts
                                               from relRightLeft in relRightLefts.DefaultIfEmpty()
                                               where relLeftRight == null && relRightLeft == null
                                               select obj).ToList();



                        var getAttributesResult = await elasticAgent.GetObjectAttributesOfObjects(objectsToDelete, pos, range);

                        result.ResultState = getAttributesResult.ResultState;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }

                        objectsToDelete = (from obj in objectsToDelete
                                           join objAtt in getAttributesResult.Result on obj.GUID equals objAtt.ID_Object into objAtts
                                           from objAtt in objAtts.DefaultIfEmpty()
                                           where objAtt == null
                                           select obj).ToList();

                        if (objectsToDelete.Any())
                        {
                            result.ResultState = await elasticAgent.DeleteObjects(objectsToDelete.Select(obj => new clsOntologyItem { GUID = obj.GUID }).ToList());
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }
                        }

                        result.Result.DeletedObjects.AddRange(objectsToDelete);
                        pos += range;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<DeleteAbandonedLeafsResult>> DeleteAbandonedLeafs(DeleteAbandonedLeafsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<DeleteAbandonedLeafsResult>>(async () =>
            {
                var result = new ResultItem<DeleteAbandonedLeafsResult>
                {
                    ResultState = Globals.LState_Success,
                    Result = new DeleteAbandonedLeafsResult()
                };

                if (string.IsNullOrEmpty(request.IdClass) || !Globals.is_GUID(request.IdClass))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Requested IdClass is not valid!";
                    return result;
                }

                if (string.IsNullOrEmpty(request.IdRelationType) || !Globals.is_GUID(request.IdRelationType))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Requested IdRelationType is not valid!";
                    return result;
                }

                if (string.IsNullOrEmpty(request.IdRelationType) || (request.IdDirection != Globals.Direction_LeftRight.GUID && request.IdDirection != Globals.Direction_RightLeft.GUID))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Requested IdDirection is not valid!";
                    return result;
                }

                var searchObjects = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = request.IdClass
                    }
                };

                var dbReaderObjects = new OntologyModDBConnector(Globals);

                result.ResultState = dbReaderObjects.GetDataObjects(searchObjects);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting Objects";
                    return result;
                }

                var searchLeftRight = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = request.IdClass
                    }
                };

                var dbReaderLeftRight = new OntologyModDBConnector(Globals);

                result.ResultState = dbReaderLeftRight.GetDataObjectRel(searchLeftRight, doIds: true);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting left-right relations";
                    return result;
                }

                var searchRightLeft = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Other = request.IdClass
                    }
                };

                var dbReaderRightLeft = new OntologyModDBConnector(Globals);

                result.ResultState = dbReaderRightLeft.GetDataObjectRel(searchRightLeft, doIds: true);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting right-left relations";
                    return result;
                }

                var leafs = new List<clsOntologyItem>();

                if (request.IdDirection == Globals.Direction_LeftRight.GUID)
                {
                    leafs = (from obj in dbReaderObjects.Objects1
                             join rel in dbReaderLeftRight.ObjectRelsId on obj.GUID equals rel.ID_Object into rels
                             from rel in rels.DefaultIfEmpty()
                             where rel == null
                             select obj).ToList();
                }
                else
                {
                    leafs = (from obj in dbReaderObjects.Objects1
                             join rel in dbReaderLeftRight.ObjectRelsId on obj.GUID equals rel.ID_Other into rels
                             from rel in rels.DefaultIfEmpty()
                             where rel == null
                             select obj).ToList();
                }

                var deletableLeafs = (from leaf in leafs
                                      join rel in dbReaderLeftRight.ObjectRelsId.Where(rel1 => rel1.ID_Parent_Other != request.IdClass) on leaf.GUID equals rel.ID_Object into rels
                                      from rel in rels.DefaultIfEmpty()
                                      where rel == null
                                      select leaf).ToList();

                deletableLeafs = (from deletableLeaf in deletableLeafs
                                  join rel in dbReaderRightLeft.ObjectRelsId.Where(rel1 => rel1.ID_Parent_Object != request.IdClass) on deletableLeaf.GUID equals rel.ID_Other into rels
                                  from rel in rels.DefaultIfEmpty()
                                  where rel == null
                                  select deletableLeaf).ToList();

                var searchAttributes = deletableLeafs.Select(leaf => new clsObjectAtt
                {
                    ID_Object = leaf.GUID
                }).ToList();

                var dbReaderAttributes = new OntologyModDBConnector(Globals);

                if (searchAttributes.Any())
                {
                    result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes, doIds: true);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting Attributes";
                        return result;
                    }
                }


                deletableLeafs = (from deletableLeaf in deletableLeafs
                                  join attribute in dbReaderAttributes.ObjAttsId on deletableLeaf.GUID equals attribute.ID_Object into attributes
                                  from attribute in attributes.DefaultIfEmpty()
                                  where attribute == null
                                  select deletableLeaf).ToList();

                result.Result.CountDeletedLeafs = deletableLeafs.Count;
                if (deletableLeafs.Any())
                {
                    var deleteRelations = deletableLeafs.Select(leaf => new clsObjectRel
                    {
                        ID_Object = leaf.GUID
                    }).ToList();

                    deleteRelations.AddRange(deletableLeafs.Select(leaf => new clsObjectRel
                    {
                        ID_Other = leaf.GUID
                    }));

                    var dbDeletor = new OntologyModDBConnector(Globals);

                    result.ResultState = dbDeletor.DelObjectRels(deleteRelations);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting Relations";
                        return result;
                    }

                    var deleteleafs = deletableLeafs.Select(obj => new clsOntologyItem
                    {
                        GUID = obj.GUID
                    }).ToList();

                    result.ResultState = dbDeletor.DelObjects(deleteleafs);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting Leafs";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<CleanupWebPostsResult>> CleanupWebPosts(CleanupWebPostsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CleanupWebPostsResult>>(() =>
           {
               var result = new ResultItem<CleanupWebPostsResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new CleanupWebPostsResult()
               };

               var searchWebPosts = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_Class = "d8782656a1e64af089b968ae9c0b81fa"
                   }
               };

               var dbReaderWebPostAttributes = new OntologyModDBConnector(Globals);

               request.MessageOutput?.OutputInfo("Get WebPost Attributes...");
               result.ResultState = dbReaderWebPostAttributes.GetDataObjectAtt(searchWebPosts);
               

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Attributes of WebPosts";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo($"Have WebPost Attributes: {dbReaderWebPostAttributes.ObjAtts.Count}");

               request.MessageOutput?.OutputInfo("Get WebPost Guids...");

               var webPostGuids = dbReaderWebPostAttributes.ObjAtts.GroupBy(objAtt => objAtt.ID_Object).Select(guidGrp => guidGrp.Key);

               request.MessageOutput?.OutputInfo($"Have Guids: {webPostGuids.Count()}");

               var dbDeletor = new OntologyModDBConnector(Globals);
               var attributesToDelete = new List<clsObjectAtt>();
               foreach (var guid in webPostGuids)
               {
                   var attributes = dbReaderWebPostAttributes.ObjAtts.Where(objAtt => objAtt.ID_Object == guid);
                   var attributesContent = attributes.Where(objAtt => objAtt.ID_AttributeType == "96f4e9490b7e4edaa094d9cb1f49f86c").ToList();
                   var attributesDateTimeStamp = attributes.Where(objAtt => objAtt.ID_AttributeType == "b67c3f3cda0346939afcd2014997e328").ToList();
                   var attributesId = attributes.Where(objAtt => objAtt.ID_AttributeType == "0b183be9c13d4157989b63b0362aeee6").ToList();
                   var attributesStatus = attributes.Where(objAtt => objAtt.ID_AttributeType == "94bf1927d98646ce9da366bf15413646").ToList();


                   for (var i = 1;i<attributesContent.Count();i++)
                   {
                       attributesToDelete.Add(new clsObjectAtt { ID_Attribute = attributesContent[i].ID_Attribute });
                   }

                   for (var i = 1; i < attributesDateTimeStamp.Count(); i++)
                   {
                       attributesToDelete.Add(new clsObjectAtt { ID_Attribute = attributesDateTimeStamp[i].ID_Attribute });
                   }

                   for (var i = 1; i < attributesId.Count(); i++)
                   {
                       attributesToDelete.Add(new clsObjectAtt { ID_Attribute = attributesId[i].ID_Attribute });
                   }

                   for (var i = 1; i < attributesStatus.Count(); i++)
                   {
                       attributesToDelete.Add(new clsObjectAtt { ID_Attribute = attributesStatus[i].ID_Attribute });
                   }
               }

               request.MessageOutput?.OutputInfo($"Delete Attributes...");
               if (attributesToDelete.Any())
               {
                   result.ResultState = dbDeletor.DelObjectAtts(attributesToDelete);
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while deleting Attributes!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
               }

               request.MessageOutput?.OutputInfo($"Deleted Attributes: {attributesToDelete.Count}");

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<CleanupMultipleItemsByNameResult>> CleanupMultipleItemsNames(CleanupMultipleItemsByNameRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CleanupMultipleItemsByNameResult>>(() =>
           {
               var result = new ResultItem<CleanupMultipleItemsByNameResult>
               {
                   ResultState = Globals.LState_Success.Clone()
               };

               var searchItems = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID_Parent = request.IdClassSource
                   }
               };

               var dbReaderSources = new OntologyModDBConnector(Globals);
               result.ResultState = dbReaderSources.GetDataObjects(searchItems);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the source-items!";
                   return result;
               }

               var sourceList = dbReaderSources.Objects1;
               var destList = new List<clsOntologyItem>();

               if (request.IdClassDest == request.IdClassSource)
               {
                   destList = sourceList;
               }
               else
               {
                   searchItems = new List<clsOntologyItem>
                   {
                       new clsOntologyItem
                       {
                           GUID_Parent = request.IdClassDest
                       }
                   };

                   var dbReaderDestItems = new OntologyModDBConnector(Globals);

                   result.ResultState = dbReaderDestItems.GetDataObjects(searchItems);

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the dest-items!";
                       return result;
                   }

                   destList = dbReaderDestItems.Objects1;
               }
               if (request.IncludeObjectIds.Any())
               {
                   destList = (from destItem in destList
                               join includeId in request.IncludeObjectIds on destItem.GUID equals includeId
                               select destItem).ToList();
               }

               if (request.ExcludeObjectIds.Any())
               {
                   destList = (from destItem in destList
                               join excludeId in request.ExcludeObjectIds on destItem.GUID equals excludeId into excludeIds
                               from excludeId in excludeIds.DefaultIfEmpty()
                               where excludeId == null
                               select destItem).ToList();
               }

               

               var destNameList = destList.GroupBy(destItem => destItem.Name).Select(dstItm => dstItm.Key).ToList();
               var mergeList = new List<MultipleItem>();
               foreach (var destItemName in destNameList.OrderBy(destItem => destItem))
               {
                   var destItem = destList.FirstOrDefault(dstItm => dstItm.Name == destItemName);

                   if (destItem != null)
                   {
                       var mergeItem = new MultipleItem
                       {
                           DestItem = destItem,
                           SourceItems = sourceList.Where(srcItm => srcItm != destItem && srcItm.Name == destItem.Name).ToList()
                       };
                       if (mergeItem.SourceItems.Any())
                       {
                           mergeList.Add(mergeItem);
                       }
                        
                   }
               }

               var relationConfig = new clsRelationConfig(Globals);
               var dbWriter = new OntologyModDBConnector(Globals);

               if (!mergeList.Any()) return result;
               if (request.MergeAttributes)
               {
                   var searchAttributes = mergeList.SelectMany(mergeItm => mergeItm.SourceItems).Select(mergeItm => new clsObjectAtt
                   {
                       ID_Object = mergeItm.GUID
                   }).ToList();

                   searchAttributes.AddRange(mergeList.Select(mergeItem => mergeItem.DestItem).Select(mergeItm => new clsObjectAtt
                   {
                       ID_Object = mergeItm.GUID
                   }));

                   var dbReaderAttributes = new OntologyModDBConnector(Globals);

                   result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Attributes!";
                       return result;
                   }

                  
                   foreach (var mergeItem in mergeList)
                   {
                       var attributesToMerge = (from sourceItem in mergeItem.SourceItems
                                                join attr in dbReaderAttributes.ObjAtts on sourceItem.GUID equals attr.ID_Object
                                                select attr);

                       var attributesInDest = dbReaderAttributes.ObjAtts.Where(att => att.ID_Object == mergeItem.DestItem.GUID);

                       var attributesToMergeBit = (from attrSrc in attributesToMerge.Where(attr => attr.ID_DataType == Globals.DType_Bool.GUID)
                                                   join attrDst in attributesInDest on new { attrSrc.ID_AttributeType, attrSrc.Val_Bit } equals new { attrDst.ID_AttributeType, attrDst.Val_Bit } into attrsDst
                                                   from attrDst in attrsDst.DefaultIfEmpty()
                                                   where attrDst == null
                                                   select attrSrc).GroupBy(attr => new { attr.ID_AttributeType, attr.Name_AttributeType, attr.ID_DataType, attr.Val_Bool }).
                                                  Select(attr => relationConfig.Rel_ObjectAttribute(mergeItem.DestItem, new clsOntologyItem
                                                  {
                                                      GUID = attr.Key.ID_AttributeType,
                                                      Name = attr.Key.Name_AttributeType,
                                                      GUID_Parent = attr.Key.ID_DataType,
                                                      Type = Globals.Type_AttributeType
                                                  }, attr.Key.Val_Bool)).ToList();


                       var attributesToMergeDateTime = (from attrSrc in attributesToMerge.Where(attr => attr.ID_DataType == Globals.DType_DateTime.GUID)
                                                        join attrDst in attributesInDest on new { attrSrc.ID_AttributeType, attrSrc.Val_Datetime } equals new { attrDst.ID_AttributeType, attrDst.Val_Datetime } into attrsDst
                                                        from attrDst in attrsDst.DefaultIfEmpty()
                                                        where attrDst == null
                                                        select attrSrc).GroupBy(attr => new { attr.ID_AttributeType, attr.Name_AttributeType, attr.ID_DataType, attr.Val_Datetime }).
                                                     Select(attr => relationConfig.Rel_ObjectAttribute(mergeItem.DestItem, new clsOntologyItem
                                                     {
                                                         GUID = attr.Key.ID_AttributeType,
                                                         Name = attr.Key.Name_AttributeType,
                                                         GUID_Parent = attr.Key.ID_DataType,
                                                         Type = Globals.Type_AttributeType
                                                     }, attr.Key.Val_Datetime)).ToList();

                       var attributesToMergeInt = (from attrSrc in attributesToMerge.Where(attr => attr.ID_DataType == Globals.DType_Int.GUID)
                                                   join attrDst in attributesInDest on new { attrSrc.ID_AttributeType, attrSrc.Val_Int } equals new { attrDst.ID_AttributeType, attrDst.Val_Int } into attrsDst
                                                   from attrDst in attrsDst.DefaultIfEmpty()
                                                   where attrDst == null
                                                   select attrSrc).GroupBy(attr => new { attr.ID_AttributeType, attr.Name_AttributeType, attr.ID_DataType, attr.Val_Int }).
                                                  Select(attr => relationConfig.Rel_ObjectAttribute(mergeItem.DestItem, new clsOntologyItem
                                                  {
                                                      GUID = attr.Key.ID_AttributeType,
                                                      Name = attr.Key.Name_AttributeType,
                                                      GUID_Parent = attr.Key.ID_DataType,
                                                      Type = Globals.Type_AttributeType
                                                  }, attr.Key.Val_Int)).ToList();

                       var attributesToMergeReal = (from attrSrc in attributesToMerge.Where(attr => attr.ID_DataType == Globals.DType_Real.GUID)
                                                    join attrDst in attributesInDest on new { attrSrc.ID_AttributeType, attrSrc.Val_Real } equals new { attrDst.ID_AttributeType, attrDst.Val_Real } into attrsDst
                                                    from attrDst in attrsDst.DefaultIfEmpty()
                                                    where attrDst == null
                                                    select attrSrc).GroupBy(attr => new { attr.ID_AttributeType, attr.Name_AttributeType, attr.ID_DataType, attr.Val_Real }).
                                                  Select(attr => relationConfig.Rel_ObjectAttribute(mergeItem.DestItem, new clsOntologyItem
                                                  {
                                                      GUID = attr.Key.ID_AttributeType,
                                                      Name = attr.Key.Name_AttributeType,
                                                      GUID_Parent = attr.Key.ID_DataType,
                                                      Type = Globals.Type_AttributeType
                                                  }, attr.Key.Val_Real)).ToList();

                       var attributesToMergeString = (from attrSrc in attributesToMerge.Where(attr => attr.ID_DataType == Globals.DType_String.GUID)
                                                      join attrDst in attributesInDest on new { attrSrc.ID_AttributeType, attrSrc.Val_String } equals new { attrDst.ID_AttributeType, attrDst.Val_String } into attrsDst
                                                      from attrDst in attrsDst.DefaultIfEmpty()
                                                      where attrDst == null
                                                      select attrSrc).GroupBy(attr => new { attr.ID_AttributeType, attr.Name_AttributeType, attr.ID_DataType, attr.Val_String }).
                                                  Select(attr => relationConfig.Rel_ObjectAttribute(mergeItem.DestItem, new clsOntologyItem
                                                  {
                                                      GUID = attr.Key.ID_AttributeType,
                                                      Name = attr.Key.Name_AttributeType,
                                                      GUID_Parent = attr.Key.ID_DataType,
                                                      Type = Globals.Type_AttributeType
                                                  }, attr.Key.Val_String)).ToList();



                       if (attributesToMergeBit.Any())
                       {
                           result.ResultState = dbWriter.SaveObjAtt(attributesToMergeBit);
                       }

                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while writing the bit-attributes";
                           return result;
                       }

                       if (attributesToMergeDateTime.Any())
                       {
                           result.ResultState = dbWriter.SaveObjAtt(attributesToMergeDateTime);
                       }

                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while writing the datetime-attributes";
                           return result;
                       }

                       if (attributesToMergeInt.Any())
                       {
                           result.ResultState = dbWriter.SaveObjAtt(attributesToMergeInt);
                       }

                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while writing the int-attributes";
                           return result;
                       }

                       if (attributesToMergeReal.Any())
                       {
                           result.ResultState = dbWriter.SaveObjAtt(attributesToMergeReal);
                       }

                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while writing the real-attributes";
                           return result;
                       }

                       if (attributesToMergeString.Any())
                       {
                           result.ResultState = dbWriter.SaveObjAtt(attributesToMergeString);
                       }

                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while writing the string-attributes";
                           return result;
                       }

                       if (attributesToMerge.Any())
                       {
                           result.ResultState = dbWriter.DelObjectAtts(attributesToMerge.ToList());
                       }

                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while deleteing old attributes";
                           return result;
                       }
                   }
               }
               else
               {
                   var searchAttributes = mergeList.SelectMany(mergeItm => mergeItm.SourceItems).Select(mergeItm => new clsObjectAtt
                   {
                       ID_Object = mergeItm.GUID
                   }).ToList();

                   searchAttributes.AddRange(mergeList.Select(mergeItem => mergeItem.DestItem).Select(mergeItm => new clsObjectAtt
                   {
                       ID_Object = mergeItm.GUID
                   }));

                   var dbReaderAttributes = new OntologyModDBConnector(Globals);

                   result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Attributes!";
                       return result;
                   }

                   if (dbReaderAttributes.ObjAtts.Any())
                   {
                       result.ResultState = dbWriter.DelObjectAtts(dbReaderAttributes.ObjAtts.Select(objAtt => new clsObjectAtt { ID_Attribute = objAtt.id }).ToList());
                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while deleting the attributes!";
                           return result;
                       }
                   }
               }

               if (request.MergeRelations)
               {
                   var searchRelations = mergeList.SelectMany(mergeItm => mergeItm.SourceItems).Select(mergeItm => new clsObjectRel
                   {
                       ID_Object = mergeItm.GUID
                   }).ToList();

                   searchRelations.AddRange(mergeList.SelectMany(mergeItm => mergeItm.SourceItems).Select(mergeItm => new clsObjectRel
                   {
                       ID_Other = mergeItm.GUID
                   }));

                   searchRelations.AddRange(mergeList.Select(mergeItem => mergeItem.DestItem).Select(mergeItm => new clsObjectRel
                   {
                       ID_Object = mergeItm.GUID
                   }));

                   searchRelations.AddRange(mergeList.Select(mergeItem => mergeItem.DestItem).Select(mergeItm => new clsObjectRel
                   {
                       ID_Other = mergeItm.GUID
                   }));

                   var dbReaderRelations = new OntologyModDBConnector(Globals);

                   result.ResultState = dbReaderRelations.GetDataObjectRel(searchRelations);

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Relations!";
                       return result;
                   }

                   foreach (var mergeItem in mergeList)
                   {
                       var relationsToMergeLeftRight = (from sourceItem in mergeItem.SourceItems
                                                join rel in dbReaderRelations.ObjectRels on sourceItem.GUID equals rel.ID_Object
                                                select rel).ToList();
                       var relationsInDestLeftRight = dbReaderRelations.ObjectRels.Where(att => att.ID_Object == mergeItem.DestItem.GUID).ToList();
                       var relationsToSave = (from relSrc in relationsToMergeLeftRight
                                              join relDst in relationsInDestLeftRight on new { relSrc.ID_Object, relSrc.ID_RelationType, relSrc.ID_Other }
                                                            equals new { relDst.ID_Object, relDst.ID_RelationType, relDst.ID_Other } into relDsts
                                              from relDst in relDsts.DefaultIfEmpty()
                                              where relDst == null
                                              select relSrc).GroupBy(rel => new { rel.ID_Object, rel.Name_Other, rel.ID_Parent_Other, rel.ID_RelationType, rel.Name_RelationType, rel.ID_Other, rel.OrderID, rel.Ontology }).Select(rel => relationConfig.Rel_ObjectRelation(mergeItem.DestItem, new clsOntologyItem
                                              {
                                                  GUID = rel.Key.ID_Other,
                                                  Name = rel.Key.Name_Other,
                                                  GUID_Parent = rel.Key.ID_Parent_Other,
                                                  Type = rel.Key.Ontology
                                              }, new clsOntologyItem
                                              {
                                                  GUID = rel.Key.ID_RelationType,
                                                  Name = rel.Key.Name_RelationType,
                                                  Type = Globals.Type_RelationType
                                              }, orderId: rel.Key.OrderID.Value)).ToList();


                       var relationsToMergeRightLeft = (from sourceItem in mergeItem.SourceItems
                                                 join rel in dbReaderRelations.ObjectRels on sourceItem.GUID equals rel.ID_Other
                                                 select rel).ToList();

                       var relationsInDestRightLeft = dbReaderRelations.ObjectRels.Where(att => att.ID_Other == mergeItem.DestItem.GUID).ToList();



                       relationsToSave.AddRange((from relSrc in relationsToMergeRightLeft
                                              join relDst in relationsInDestRightLeft on new { relSrc.ID_Object, relSrc.ID_RelationType, relSrc.ID_Other }
                                                            equals new { relDst.ID_Object, relDst.ID_RelationType, relDst.ID_Other } into relDsts
                                              from relDst in relDsts.DefaultIfEmpty()
                                              where relDst == null
                                              select relSrc).GroupBy(rel => new { rel.ID_Object, rel.Name_Object, rel.ID_Parent_Object, rel.ID_RelationType, rel.Name_RelationType, rel.ID_Other, rel.OrderID, rel.Ontology }).Select(rel => relationConfig.Rel_ObjectRelation(new clsOntologyItem
                                              {
                                                  GUID = rel.Key.ID_Object,
                                                  Name = rel.Key.Name_Object,
                                                  GUID_Parent = rel.Key.ID_Parent_Object,
                                                  Type = rel.Key.Ontology
                                              }, 
                                              mergeItem.DestItem, 
                                              new clsOntologyItem
                                              {
                                                  GUID = rel.Key.ID_RelationType,
                                                  Name = rel.Key.Name_RelationType,
                                                  Type = Globals.Type_RelationType
                                              }, orderId: rel.Key.OrderID.Value)));


                       if (relationsToSave.Any())
                       {
                           result.ResultState = dbWriter.SaveObjRel(relationsToSave);
                           if (result.ResultState.GUID == Globals.LState_Error.GUID)
                           {
                               result.ResultState.Additional1 = "Error while saving the relations!";
                               return result;
                           }
                       }

                       var relsToDelete = relationsToMergeLeftRight.Select(rel => new clsObjectRel { ID_Object = rel.ID_Object, ID_RelationType = rel.ID_RelationType, ID_Other = rel.ID_Other }).ToList();
                       relsToDelete.AddRange(relationsToMergeRightLeft.Select(rel => new clsObjectRel { ID_Object = rel.ID_Object, ID_RelationType = rel.ID_RelationType, ID_Other = rel.ID_Other }));

                       if (relsToDelete.Any())
                       {
                           result.ResultState = dbWriter.DelObjectRels(relsToDelete);

                           if (result.ResultState.GUID == Globals.LState_Error.GUID)
                           {
                               result.ResultState.Additional1 = "Error while deleting the relations!";
                               return result;
                           }
                       }
                   }
               }
               else
               {
                   var searchRelations = mergeList.SelectMany(mergeItm => mergeItm.SourceItems).Select(mergeItm => new clsObjectRel
                   {
                       ID_Object = mergeItm.GUID
                   }).ToList();

                   searchRelations.AddRange(mergeList.SelectMany(mergeItm => mergeItm.SourceItems).Select(mergeItm => new clsObjectRel
                   {
                       ID_Other = mergeItm.GUID
                   }));

                   searchRelations.AddRange(mergeList.Select(mergeItem => mergeItem.DestItem).Select(mergeItm => new clsObjectRel
                   {
                       ID_Object = mergeItm.GUID
                   }));

                   searchRelations.AddRange(mergeList.Select(mergeItem => mergeItem.DestItem).Select(mergeItm => new clsObjectRel
                   {
                       ID_Other = mergeItm.GUID
                   }));

                   var dbReaderRelations = new OntologyModDBConnector(Globals);

                   result.ResultState = dbReaderRelations.GetDataObjectRel(searchRelations);

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Relations!";
                       return result;
                   }

                   var relationsToDelete = dbReaderRelations.ObjectRels.Select(rel => new clsObjectRel
                   {
                       ID_Object = rel.ID_Object,
                       ID_RelationType = rel.ID_RelationType,
                       ID_Other = rel.ID_Other
                   }).ToList();

                   if (relationsToDelete.Any())
                   {
                       result.ResultState = dbWriter.DelObjectRels(relationsToDelete);
                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while deleting the relations!";
                           return result;
                       }
                   }
                   
               }

               var objectsToDelete = mergeList.SelectMany(mergeItem => mergeItem.SourceItems).ToList();

               if (objectsToDelete.Any())
               {
                   result.ResultState = dbWriter.DelObjects(objectsToDelete);
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while deliting objects!";
                       return result;
                   }
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<CleanupMultipleGUIDsResult>> CleanupMultipleGUIDs(CleanupMultipleGUIDsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CleanupMultipleGUIDsResult>>(async () =>
           {
               var result = new ResultItem<CleanupMultipleGUIDsResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new CleanupMultipleGUIDsResult()
               };

               var dbReaderClasses = new OntologyModDBConnector(Globals);

               var searchClasses = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdClass
                   }

               };

               result.ResultState = dbReaderClasses.GetDataClasses(searchClasses);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while reading Classes!";
                   return result;
               }

               var dbReaderObjects = new OntologyModDBConnector(Globals);

               foreach (var clsItem in dbReaderClasses.Classes1)
               {
                   var searchObjects = new List<clsOntologyItem>
                   {
                       new clsOntologyItem
                       {
                           GUID_Parent = clsItem.GUID
                       }
                   };

                   result.ResultState = dbReaderObjects.GetDataObjects(searchObjects);

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while reading Objects";
                       return result;
                   }

                   var multipleGUIDS = dbReaderObjects.Objects1.GroupBy(obj => obj.GUID.ToLower()).Select(objGrp => new { GUID = objGrp.Key, Count = objGrp.Count() }).Where(multiple => multiple.Count > 1).ToList();

                   result.Result.MultipleGUIDs.AddRange(multipleGUIDS.Where(multiple => !string.IsNullOrEmpty(multiple.GUID)).Select(multiple => new clsOntologyItem
                   {
                       GUID = multiple.GUID,
                       GUID_Parent = clsItem.GUID,
                       Val_Long = multiple.Count
                   }));

                   if (!request.Check)
                   {
                       var deleteObjects = multipleGUIDS.Where(multiple => !string.IsNullOrEmpty(multiple.GUID)).Select(multiple => new clsOntologyItem
                       {
                           GUID = multiple.GUID
                       }).ToList();

                       var deleteRequest = new DeleteObjectsAndRelationsRequest
                       {
                           ObjectsToDelete = deleteObjects
                       };
                       var oitemListController = new OItemListController(Globals);
                       var deleteResult = await oitemListController.DeleteObjectsAndRelations(deleteRequest);

                       result.ResultState = deleteResult.ResultState;

                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           return result;
                       }

                   }
               }


               return result;
           });

            return taskResult;
        }

       
        public async Task<ResultItem<ConvertIntIdToGuidHashResult>> ConvertIntIdToGuid(ConvertIntIdToGuidHashRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ConvertIntIdToGuidHashResult>>(() =>
            {
                var result = new ResultItem<ConvertIntIdToGuidHashResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ConvertIntIdToGuidHashResult()
                };

                if (request.QueryItem == null)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "You have to provide a QueryItem";
                    return result;
                }

                var searchItems = new List<clsOntologyItem>
                {
                    request.QueryItem
                };

                var dbReaderBaseList = new OntologyModDBConnector(Globals);


                result.ResultState = dbReaderBaseList.GetDataObjects(searchItems);

                result.Result.QueryCount = dbReaderBaseList.Objects1.Count;

                int test = 0;
                var objectsOldNew = dbReaderBaseList.Objects1.Where(obj => int.TryParse(obj.GUID, out test)).Select(oldObj =>
                {
                    var newObj = new clsOntologyItem
                    {
                        GUID = MD5Converters.CalculateMD5Hash(oldObj.GUID),
                        Name = oldObj.Name,
                        GUID_Parent = oldObj.GUID_Parent,
                        Type = oldObj.Type
                    };

                    var resultOldNewObj = new { oldObj, newObj };

                    return resultOldNewObj;
                }).ToList();

                result.Result.QueryCount = objectsOldNew.Count;

                var searchLeftRight = objectsOldNew.Select(objOldNew => new clsObjectRel
                {
                    ID_Object = objOldNew.oldObj.GUID
                }).ToList();

                var searchRightLeft = objectsOldNew.Select(objOldNew => new clsObjectRel
                {
                    ID_Other = objOldNew.oldObj.GUID
                }).ToList();

                var searchAttributes = objectsOldNew.Select(objOldNew => new clsObjectAtt
                {
                    ID_Object = objOldNew.oldObj.GUID
                }).ToList();

                var dbReaderRelLeftRight = new OntologyModDBConnector(Globals);

                var dbReaderRelRightLeft = new OntologyModDBConnector(Globals);

                var dbReaderAttributes = new OntologyModDBConnector(Globals);

                if (searchLeftRight.Any())
                {
                    result.ResultState = dbReaderRelLeftRight.GetDataObjectRel(searchLeftRight);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting Left-Right Relations";
                        return result;
                    }
                }


                if (searchRightLeft.Any())
                {
                    result.ResultState = dbReaderRelRightLeft.GetDataObjectRel(searchRightLeft);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting Right-Left Relations";
                        return result;
                    }
                }

                if (searchAttributes.Any())
                {
                    result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting Right-Left Relations";
                        return result;
                    }
                }

                var newRelations = (from objOldNew in objectsOldNew
                                    join objLeftRight in dbReaderRelLeftRight.ObjectRels on objOldNew.oldObj.GUID equals objLeftRight.ID_Object
                                    select new { objOldNew, objLeftRight }).Select(obj =>
                                      {
                                          var relation = new clsObjectRel
                                          {
                                              ID_Object = obj.objOldNew.newObj.GUID,
                                              ID_Parent_Object = obj.objOldNew.newObj.GUID_Parent,
                                              ID_RelationType = obj.objLeftRight.ID_RelationType,
                                              ID_Other = obj.objLeftRight.ID_Other,
                                              ID_Parent_Other = obj.objLeftRight.ID_Parent_Other,
                                              Ontology = obj.objLeftRight.Ontology,
                                              OrderID = obj.objLeftRight.OrderID
                                          };
                                          return relation;
                                      }).ToList();

                newRelations.AddRange((from objOldNew in objectsOldNew
                                       join objLeftRight in dbReaderRelRightLeft.ObjectRels on objOldNew.oldObj.GUID equals objLeftRight.ID_Other
                                       select new { objOldNew, objLeftRight }).Select(obj =>
                                       {
                                           var relation = new clsObjectRel
                                           {
                                               ID_Object = obj.objOldNew.newObj.GUID,
                                               ID_Parent_Object = obj.objOldNew.newObj.GUID_Parent,
                                               ID_RelationType = obj.objLeftRight.ID_RelationType,
                                               ID_Other = obj.objLeftRight.ID_Other,
                                               ID_Parent_Other = obj.objLeftRight.ID_Parent_Other,
                                               Ontology = obj.objLeftRight.Ontology,
                                               OrderID = obj.objLeftRight.OrderID
                                           };
                                           return relation;
                                       }));

                var newAttributes = (from objOldNew in objectsOldNew
                                     join objAttribute in dbReaderAttributes.ObjAtts on objOldNew.oldObj.GUID equals objAttribute.ID_Object
                                     select new { objOldNew, objAttribute }).Select(obj =>
                                      {
                                          var relation = new clsObjectAtt
                                          {
                                              ID_Attribute = Globals.NewGUID,
                                              ID_AttributeType = obj.objAttribute.ID_AttributeType,
                                              ID_DataType = obj.objAttribute.ID_DataType,
                                              ID_Object = obj.objOldNew.newObj.GUID,
                                              ID_Class = obj.objOldNew.newObj.GUID_Parent,
                                              OrderID = obj.objAttribute.OrderID,
                                              Val_Bit = obj.objAttribute.Val_Bit,
                                              Val_Datetime = obj.objAttribute.Val_Date,
                                              Val_Lng = obj.objAttribute.Val_Lng,
                                              Val_Name = obj.objAttribute.Val_Name,
                                              Val_Real = obj.objAttribute.Val_Real,
                                              Val_String = obj.objAttribute.Val_String
                                          };
                                          return relation;
                                      }).ToList();

                var dbWriterDeletor = new OntologyModDBConnector(Globals);

                result.Result.CreateCountObjects = objectsOldNew.Count;
                if (objectsOldNew.Any())
                {
                    result.ResultState = dbWriterDeletor.SaveObjects(objectsOldNew.Select(obj => obj.newObj).ToList());
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving new objects";
                        return result;
                    }
                }

                result.Result.CreateCountObjectAttributes = newAttributes.Count;
                if (newAttributes.Any())
                {
                    result.ResultState = dbWriterDeletor.SaveObjAtt(newAttributes);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving new attributes";
                        return result;
                    }
                }


                result.Result.CreateCountObjectRelations = newRelations.Count;
                if (newRelations.Any())
                {
                    result.ResultState = dbWriterDeletor.SaveObjRel(newRelations);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving new relations";
                        return result;
                    }
                }

                result.Result.DelCountObjectAttributes = dbReaderAttributes.ObjAtts.Count;
                if (dbReaderAttributes.ObjAtts.Any())
                {
                    result.ResultState = dbWriterDeletor.DelObjectAtts(dbReaderAttributes.ObjAtts.Select(objAtt => new clsObjectAtt
                    {
                        ID_Attribute = objAtt.ID_Attribute
                    }).ToList());

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting old attributes";
                        return result;
                    }
                }


                result.Result.DelCountObjectRelations += dbReaderRelLeftRight.ObjectRels.Count;
                if (dbReaderRelLeftRight.ObjectRels.Any())
                {
                    result.ResultState = dbWriterDeletor.DelObjectRels(dbReaderRelLeftRight.ObjectRels.Select(objRel => new clsObjectRel
                    {
                        ID_Object = objRel.ID_Object,
                        ID_RelationType = objRel.ID_RelationType,
                        ID_Other = objRel.ID_Other
                    }).ToList());
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting old relations (left-right)";
                        return result;
                    }
                }

                result.Result.DelCountObjectRelations += dbReaderRelRightLeft.ObjectRels.Count;
                if (dbReaderRelRightLeft.ObjectRels.Any())
                {
                    result.ResultState = dbWriterDeletor.DelObjectRels(dbReaderRelRightLeft.ObjectRels.Select(objRel => new clsObjectRel
                    {
                        ID_Object = objRel.ID_Object,
                        ID_RelationType = objRel.ID_RelationType,
                        ID_Other = objRel.ID_Other
                    }).ToList());
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting old relations (right-left)";
                        return result;
                    }
                }


                result.Result.DelCountObjects += objectsOldNew.Count;
                if (objectsOldNew.Any())
                {
                    result.ResultState = dbWriterDeletor.DelObjects(objectsOldNew.Select(obj => new clsOntologyItem
                    {
                        GUID = obj.oldObj.GUID
                    }).ToList());
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting old objects";
                        return result;
                    }
                }


                return result;
            });

            return taskResult;
        }


        public async Task<ResultItem<RelationsWithoutRelationTypesResult>> GetRelationsWithoutRelationTypes()
        {
            var taskResult = await Task.Run<ResultItem<RelationsWithoutRelationTypesResult>>(() =>
            {
                var result = new ResultItem<RelationsWithoutRelationTypesResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new RelationsWithoutRelationTypesResult()
                };

                var dbReaderRelationTypes = new OntologyModDBConnector(Globals);

                result.ResultState = dbReaderRelationTypes.GetDataRelationTypes(null);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the RelationTypes!";
                    return result;
                }

                var dbReader = new clsUserAppDBSelector(Globals.Server, Globals.Port, Globals.Index, Globals.SearchRange, Globals.Session);

                var query = "*";

                

                var nonExistingClassRelations = new List<clsClassRel>();
                var nonExistingObjectRelations = new List<clsObjectRel>();

                var page = 0;
                var queryResult = dbReader.GetData_Documents(500, Globals.Index, "ClassRel", query, page: page);

                while (queryResult.IsOK && queryResult.Documents.Count > 0)
                {
                    page++;

                    nonExistingClassRelations.AddRange(from doc in queryResult.Documents
                                                        join relationType in dbReaderRelationTypes.RelationTypes on doc.Dict[nameof(clsClassRel.ID_RelationType)].ToString() equals relationType.GUID into relationTypes
                                                        from relationType in relationTypes.DefaultIfEmpty()
                                                        where relationType == null
                                                        select new clsClassRel
                                                        {
                                                            ID_Class_Left = doc.Dict.ContainsKey(nameof(clsClassRel.ID_Class_Left)) ? doc.Dict[nameof(clsClassRel.ID_Class_Left)].ToString() : null,
                                                            ID_Class_Right = doc.Dict.ContainsKey(nameof(clsClassRel.ID_Class_Right)) ? doc.Dict[nameof(clsClassRel.ID_Class_Right)].ToString() : null,
                                                            ID_RelationType = doc.Dict.ContainsKey(nameof(clsClassRel.ID_RelationType)) ? doc.Dict[nameof(clsClassRel.ID_RelationType)].ToString() : null,
                                                            Ontology = doc.Dict.ContainsKey(nameof(clsClassRel.Ontology)) ? doc.Dict[nameof(clsClassRel.Ontology)].ToString() : null,
                                                            Min_Forw = doc.Dict.ContainsKey(nameof(clsClassRel.Min_Forw)) ? (long?)doc.Dict[nameof(clsClassRel.Min_Forw)] : null,
                                                            Max_Forw = doc.Dict.ContainsKey(nameof(clsClassRel.Max_Forw)) ? (long?)doc.Dict[nameof(clsClassRel.Max_Forw)] : null,
                                                            Max_Backw = doc.Dict.ContainsKey(nameof(clsClassRel.Max_Backw)) ? (long?)doc.Dict[nameof(clsClassRel.Max_Backw)] : null
                                                        });

                    queryResult = dbReader.GetData_Documents(500, Globals.Index, "ClassRel", query, scrollId: queryResult.ScrollId,page:page);

                    
                }

                page = 0;
                queryResult = dbReader.GetData_Documents(500, Globals.Index, "ObjectRel", query, page: page);
                while (queryResult.IsOK && queryResult.Documents.Count > 0)
                {
                    page++;

                    nonExistingObjectRelations.AddRange(from doc in queryResult.Documents
                                                       join relationType in dbReaderRelationTypes.RelationTypes on doc.Dict[nameof(clsObjectRel.ID_RelationType)].ToString() equals relationType.GUID into relationTypes
                                                       from relationType in relationTypes.DefaultIfEmpty()
                                                       where relationType == null
                                                       select new clsObjectRel
                                                       {
                                                           ID_Object = doc.Dict.ContainsKey(nameof(clsObjectRel.ID_Object)) ? doc.Dict[nameof(clsObjectRel.ID_Object)].ToString() : null,
                                                           ID_Parent_Object = doc.Dict.ContainsKey(nameof(clsObjectRel.ID_Parent_Object)) ? doc.Dict[nameof(clsObjectRel.ID_Parent_Object)].ToString() : null,
                                                           ID_RelationType = doc.Dict.ContainsKey(nameof(clsObjectRel.ID_RelationType)) ? doc.Dict[nameof(clsObjectRel.ID_RelationType)].ToString() : null,
                                                           ID_Other = doc.Dict.ContainsKey(nameof(clsObjectRel.ID_Other)) ? doc.Dict[nameof(clsObjectRel.ID_Other)].ToString() : null,
                                                           ID_Parent_Other = doc.Dict.ContainsKey(nameof(clsObjectRel.ID_Parent_Other)) ? doc.Dict[nameof(clsObjectRel.ID_Parent_Other)].ToString() : null,
                                                           Ontology = doc.Dict.ContainsKey(nameof(clsObjectRel.Ontology)) ? doc.Dict[nameof(clsObjectRel.Ontology)].ToString() : null,
                                                           OrderID = doc.Dict.ContainsKey(nameof(clsObjectRel.OrderID)) ? (long?)doc.Dict[nameof(clsObjectRel.OrderID)] : null
                                                       });

                    queryResult = dbReader.GetData_Documents(500, Globals.Index, "ObjectRel", query, scrollId: queryResult.ScrollId, page: page);


                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<RelateObjectsWithObjectsByNameToAttributeResult>> RelateObjectsWithObjectsByNameToAttribute(RelateObjectsWithObjectsByNameToAttributeRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<RelateObjectsWithObjectsByNameToAttributeResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new RelateObjectsWithObjectsByNameToAttributeResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = Validation.ValidationController.ValidateRelateObjectsWithObjectsByNameToAttributeRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                var searchSourceObjects = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = request.IdClassSource
                    }
                };

                var elasticAgent = new ServiceAgentElastic(Globals);

                request.MessageOutput?.OutputInfo("Search source-objects...");

                var sourceObjectsResult = await elasticAgent.GetObjects(searchSourceObjects);

                result.ResultState = sourceObjectsResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the source-objects";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var sourceObjects = sourceObjectsResult.Result;

                request.MessageOutput?.OutputInfo($"Found {sourceObjects.Count} source-objects.");

                var searchAttributes = sourceObjects.Select(obj => new clsObjectAtt
                {
                    ID_AttributeType = request.IdAttributeTypeDest,
                    ID_Class = request.IdClassDest
                }).ToList();

                request.MessageOutput?.OutputInfo("Search dest-attributes...");

                var objectAttributeMap = new List<ObjectAttributeMap>();

                if (searchAttributes.Any())
                {
                    var destAttributesResult = await elasticAgent.GetAttributes(searchAttributes);

                    result.ResultState = destAttributesResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the source-attributes";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    objectAttributeMap = (from obj in sourceObjects
                                      join destAttribute in destAttributesResult.Result on obj.Name equals destAttribute.Val_String
                                      select new ObjectAttributeMap
                                      {
                                          ObjectItem = obj,
                                          Attribute = destAttribute
                                      }).ToList();
                }

                var relationConfig = new clsRelationConfig(Globals);

                var searchRelated = sourceObjects.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_Parent_Other = request.IdClassDest
                }).ToList();

                var sourceDestRelationResult = await elasticAgent.GetRelationItems(searchRelated);
                result.ResultState = sourceDestRelationResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the source-dest-relations";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var toRelate = (from foundAttributeMap in objectAttributeMap
                               join sourceDestMap in sourceDestRelationResult.Result on new { IdSourceObject = foundAttributeMap.ObjectItem.GUID, IdDestObject = foundAttributeMap.Attribute.ID_Object }
                                                                                 equals new { IdSourceObject = sourceDestMap.ID_Object, IdDestObject = sourceDestMap.ID_Other } into sourceDestMaps
                               from sourceDestMap in sourceDestMaps.DefaultIfEmpty()
                               where sourceDestMap == null
                               select foundAttributeMap).ToList();

                var relationRequest = new List<clsObjectRel>();

                foreach (var relationPre in toRelate)
                {
                    var destObject = new clsOntologyItem
                    {
                        GUID = relationPre.Attribute.ID_Object,
                        Name = relationPre.Attribute.Name_Object,
                        GUID_Parent = relationPre.Attribute.ID_Class,
                        Type = Globals.Type_Object
                    };


                    if (request.Direction.GUID == Globals.Direction_LeftRight.GUID)
                    {
                        relationRequest.Add(relationConfig.Rel_ObjectRelation(relationPre.ObjectItem, destObject, request.RelationType));
                    }
                    else
                    {
                        relationRequest.Add(relationConfig.Rel_ObjectRelation(destObject, relationPre.ObjectItem, request.RelationType));
                    }
                }
                if (relationRequest.Any())
                {
                    var saveResult = await elasticAgent.SaveRelations(relationRequest);
                    result.ResultState = saveResult;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saveing the source-dest-relations";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }

                request.MessageOutput?.OutputInfo($"Have {objectAttributeMap.Count} dest-attributes.");
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> RelateSynonymsToElementsUsing(IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var searchReplaceLogsToSynonyms = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Other = "af723ff6d48747cf86cac746c6fbc619",   // DB-Synonyms
                        ID_RelationType = Globals.RelationType_belongsTo.GUID,
                        ID_Parent_Object = "18d752ff00ab45e682cab0d4ae70682d"   //
                    }
                };

                var dbReaderReplaceLogToSynonyms = new OntologyModDBConnector(Globals);

                messageOutput?.OutputInfo("Get Replace-Logs to Snyonyms...");
                result.ResultState = dbReaderReplaceLogToSynonyms.GetDataObjectRel(searchReplaceLogsToSynonyms);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the relations between Replace-Logs and Synonyms!";
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo($"Have {dbReaderReplaceLogToSynonyms.ObjectRels.Count} relations.");

                var searchObjectsToCheck = dbReaderReplaceLogToSynonyms.ObjectRels
                    .Select(rel => rel.Name_Object.Substring(0, rel.Name_Object.IndexOf(".")))
                    .GroupBy(relGrp => relGrp).Select(relG => new clsOntologyItem
                    {
                        Name = relG.Key,
                        GUID_Parent = "c4d7533a9bcb42bca66695f2a0eb60c5",
                        Type = Globals.Type_Object
                    }).ToList();

                var dbConnectorObjects = new OntologyModDBConnector(Globals);

                messageOutput?.OutputInfo($"Search for {searchObjectsToCheck.Count} Database-Elements...");

                if (searchObjectsToCheck.Any())
                {
                    result.ResultState = dbConnectorObjects.GetDataObjects(searchObjectsToCheck);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting existing Database-Elements!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }
                
                (from searchObject in searchObjectsToCheck
                                      join existObject in dbConnectorObjects.Objects1 on searchObject.Name equals existObject.Name into existObjects
                                      from existObject in existObjects.DefaultIfEmpty()
                                      select new { searchObject, existObject }).ToList().ForEach(objObj =>
                                      {
                                          if (objObj.existObject == null)
                                          {
                                              objObj.searchObject.GUID = Globals.NewGUID;
                                              objObj.searchObject.Mark = true;
                                          }
                                          else
                                          {
                                              objObj.searchObject.GUID = objObj.existObject.GUID;
                                              objObj.searchObject.Mark = false;
                                          }
                                      });

                var objectsToSave = searchObjectsToCheck.Where(obj => obj.Mark.Value).ToList();

                messageOutput?.OutputInfo($"Have {searchObjectsToCheck.Count} Database-Elements and {objectsToSave.Count} new Elements.");

                if (objectsToSave.Any())
                {
                    result.ResultState = dbConnectorObjects.SaveObjects(objectsToSave);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the new Database-Elements!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }

                messageOutput?.OutputInfo($"Saved {objectsToSave.Count} Database-Elements.");
                var relationConfig = new clsRelationConfig(Globals);

                var relationsToCheck = (from objCheck in searchObjectsToCheck
                                        join logToSynonym in dbReaderReplaceLogToSynonyms.ObjectRels on objCheck.Name equals logToSynonym.Name_Object.Substring(0, logToSynonym.Name_Object.IndexOf("."))
                                        select relationConfig.Rel_ObjectRelation(new clsOntologyItem
                                        {
                                            GUID = logToSynonym.ID_Other,
                                            Name = logToSynonym.Name_Other,
                                            GUID_Parent = logToSynonym.ID_Parent_Other,
                                            Type = logToSynonym.Ontology
                                        }, objCheck, new clsOntologyItem
                                        {
                                            GUID = "1f063a79ddde44c595ba50391fe6f0e3",
                                            Name = "is used by",
                                            Type = Globals.Type_RelationType
                                        }, orderId: 1)).ToList();

                var searchRelations = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Other= "c4d7533a9bcb42bca66695f2a0eb60c5",
                        ID_RelationType = "1f063a79ddde44c595ba50391fe6f0e3",
                        ID_Parent_Object = "af723ff6d48747cf86cac746c6fbc619"
                    }
                };

                messageOutput?.OutputInfo($"Check {relationsToCheck.Count} relations between Synonyms and Database-Elements...");

                var dbConnectorSynonymToDatabaseElement = new OntologyModDBConnector(Globals);

                if (relationsToCheck.Any())
                {
                    result.ResultState = dbConnectorSynonymToDatabaseElement.GetDataObjectRel(searchRelations);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the new Database-Relations!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }

                messageOutput?.OutputInfo($"Check relations between Synonyms and Database-Elements.");

                var relationsToSave = (from relToCheck in relationsToCheck
                                       join relExist in dbConnectorSynonymToDatabaseElement.ObjectRels on new { relToCheck.ID_Object, relToCheck.ID_RelationType, relToCheck.ID_Other }
                                                                                                equals new { relExist.ID_Object, relExist.ID_RelationType, relExist.ID_Other } into relExists
                                       from relExist in relExists.DefaultIfEmpty()
                                       where relExist == null
                                       select relToCheck).ToList();

                var relationsToDelete = (from relExist in dbConnectorSynonymToDatabaseElement.ObjectRels
                                         join relToCheck in relationsToCheck on new { relExist.ID_Object, relExist.ID_RelationType, relExist.ID_Other }
                                                                         equals new { relToCheck.ID_Object, relToCheck.ID_RelationType, relToCheck.ID_Other } into relsToCheck
                                         from relToCheck in relsToCheck.DefaultIfEmpty()
                                         where relToCheck == null
                                         select relExist).Select(rel => new clsObjectRel
                                         {
                                             ID_Object = rel.ID_Object,
                                             ID_RelationType = rel.ID_RelationType,
                                             ID_Other = rel.ID_Other
                                         }).ToList();

                messageOutput?.OutputInfo($"Save {relationsToSave.Count} relations between Synonyms and Database-Elements...");
                if (relationsToSave.Any())
                {
                    result.ResultState = dbConnectorSynonymToDatabaseElement.SaveObjRel(relationsToSave);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving relations between Synonyms and Database-Elements!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }
                messageOutput?.OutputInfo($"Saved relations between Synonyms and Database-Elements...");

                messageOutput?.OutputInfo($"Delete {relationsToDelete.Count} relations between Synonyms and Database-Elements...");
                if (relationsToDelete.Any())
                {
                    
                    result.ResultState = dbConnectorSynonymToDatabaseElement.DelObjectRels(relationsToDelete);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting relations between Synonyms and Database-Elements!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    
                }
                messageOutput?.OutputInfo($"Deleted relations between Synonyms and Database-Elements...");

                result.Result = relationsToSave;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<MapPathToApplicationByReplaceLogResult>> MapPathToApplicationByReplaceLog(IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<MapPathToApplicationByReplaceLogResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new MapPathToApplicationByReplaceLogResult()
                };

                var elasticAgent = new ServiceAgentElastic(Globals);

                messageOutput?.OutputInfo("Get MapPathToApplicationByReplaceLog model ...");
                var modelResult = await elasticAgent.GetMapPathToApplicationByReplaceModel();

                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo("Have MapPathToApplicationByReplaceLog model.");

                messageOutput?.OutputInfo("Get Paths to check...");
                var rootPathToApplicationMap = (from pathToApplication in modelResult.Result.ReplaceLogsToApplications
                                                join replaced in modelResult.Result.ReplaceLogsReplaced on pathToApplication.ID_Object equals replaced.ID_Object
                                                join path in modelResult.Result.ReplaceLogsPaths on pathToApplication.ID_Object equals path.ID_Object
                                                where replaced.Val_Bool.Value
                                                select new { pathToApplication, path }).Select(pathToApplication => new
                                                {
                                                    IdApplication = pathToApplication.pathToApplication.ID_Other,
                                                    NameApplication = pathToApplication.pathToApplication.Name_Other,
                                                    Path = Path.GetDirectoryName(pathToApplication.path.Val_String)
                                                }).GroupBy(appPath => new { appPath.IdApplication, appPath.NameApplication, appPath.Path }).Select(grp => new { grp.Key.IdApplication, grp.Key.NameApplication, grp.Key.Path }).ToList();

                var pathsToCheck = rootPathToApplicationMap.GroupBy(grp => grp.Path).Select(pathItm => pathItm.Key).Select(pathItm => new clsOntologyItem
                {
                    Name = pathItm,
                    GUID_Parent = MaintainenceModule.MapPathToApplicationByReplaceLog.Config.LocalData.Class_Path.GUID,
                    Type = Globals.Type_Object
                }).ToList();

                messageOutput?.OutputInfo($"Have {pathsToCheck.Count} Paths to check.");

                messageOutput?.OutputInfo("Check Paths and create if not present...");
                var checkObjects = await elasticAgent.CheckObjects(pathsToCheck, MaintainenceModule.MapPathToApplicationByReplaceLog.Config.LocalData.Class_Path.GUID);

                result.ResultState = checkObjects.ResultState;

                messageOutput?.OutputInfo($"Checked and created {checkObjects.Result.Where(obj => obj.New_Item.Value).Count()} objects.");

                var relationConfig = new clsRelationConfig(Globals);

                messageOutput?.OutputInfo($"Get relations between Applications and Paths to be checked...");
                var relationsToCheck = (from map in rootPathToApplicationMap
                                         join pathObject in checkObjects.Result on map.Path equals pathObject.Name
                                         select relationConfig.Rel_ObjectRelation(new clsOntologyItem
                                         {
                                             GUID = map.IdApplication,
                                             Name = map.NameApplication,
                                             GUID_Parent = MaintainenceModule.MapPathToApplicationByReplaceLog.Config.LocalData.Class_Application.GUID,
                                             Type = Globals.Type_Object
                                         },
                                         pathObject,
                                         MaintainenceModule.MapPathToApplicationByReplaceLog.Config.LocalData.RelationType_belonging_Resources)).ToList();


                messageOutput?.OutputInfo($"Have { relationsToCheck.Count } relations between Applications and Paths to be checked.");
                var searchRelations = relationsToCheck.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_Other = rel.ID_Other,
                    ID_RelationType = rel.ID_RelationType
                }).ToList();

                var existingAppToPath = await elasticAgent.GetRelationItems(searchRelations);
                result.ResultState = existingAppToPath.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var relationsToCreate = (from rel in relationsToCheck
                                         join existingRel in existingAppToPath.Result on new { rel.ID_Object, rel.ID_RelationType, rel.ID_Other }
                                                                                  equals new { existingRel.ID_Object, existingRel.ID_RelationType, existingRel.ID_Other } into existingRels
                                         from existingRel in existingRels.DefaultIfEmpty()
                                         where existingRel == null
                                         select rel).ToList();

                messageOutput?.OutputInfo($"Create { relationsToCreate.Count } relations between Applications and Paths...");

                var createResult = await elasticAgent.SaveRelations(relationsToCreate);
                result.ResultState = createResult;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while creating the relations between Applications and Paths!";
                    return result;
                }

                messageOutput?.OutputInfo($"Created relations between Applications and Paths.");

              



                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<RepairAttributesAndRelationsResult>> RepairAttributesAndRelations(RepairAttributesAndRelationsRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<RepairAttributesAndRelationsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new RepairAttributesAndRelationsResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateRepairAttributesAndRelationsRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                var elasticAgent = new ServiceAgentElastic(Globals);

                var objectsSearch = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = request.IdClassToRepairObjects
                    }
                };

                request.MessageOutput?.OutputInfo("Get objects...");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "Cancelled by User!";
                    return result;
                }

                var objectSearchResult = await elasticAgent.GetObjects(objectsSearch);

                result.ResultState = objectSearchResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = $"Error while getting the objects of class with id {request.IdClassToRepairObjects}!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var skip = 0;
                var count = objectSearchResult.Result.Count;
                var pageSize = 100;

                result.Result.CountObjects = objectSearchResult.Result.Count;

                while (count > 0)
                {
                    if (request.CancellationToken.IsCancellationRequested)
                    {
                        result.ResultState.Additional1 = "Cancelled by User!";
                        request.MessageOutput?.OutputWarning(result.ResultState.Additional1);
                        return result;
                    }

                    if (count < 100)
                    {
                        pageSize = count;
                    }

                    var objectsToCheck = objectSearchResult.Result.Skip(skip).Take(pageSize).ToList();

                    var searchAttributes = objectsToCheck.Select(obj => new clsObjectAtt
                    {
                        ID_Object = obj.GUID
                    }).ToList();

                    var attributesGetResult = await elasticAgent.GetAttributes(searchAttributes, doIds:true);
                    result.ResultState = attributesGetResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Attributes of Objects!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    result.Result.CountAttributes += attributesGetResult.Result.Count;
                    var attributesToSave = attributesGetResult.Result.Where(att => att.ID_Class != request.IdClassToRepairObjects).ToList();

                    if (attributesToSave.Any())
                    {
                        foreach (var attribute in attributesToSave)
                        {
                            attribute.ID_Class = request.IdClassToRepairObjects;
                        }

                        var saveResult = await elasticAgent.SaveAttributes(attributesToSave);

                        result.ResultState = saveResult;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the Attributes!";
                        }
                        result.Result.CountAttributesChanged += attributesToSave.Count;
                    }

                    var searchLeftRightRelations = objectsToCheck.Select(obj => new clsObjectRel
                    {
                        ID_Object = obj.GUID
                    }).ToList();

                    var relationsToCheckResult = await elasticAgent.GetRelationItems(searchLeftRightRelations, doIds: true);
                    result.ResultState = relationsToCheckResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Left-Right relations of the objects!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    result.Result.CountRelations += relationsToCheckResult.Result.Count;
                    var relationsToSave = relationsToCheckResult.Result.Where(rel => rel.ID_Parent_Object != request.IdClassToRepairObjects).ToList();

                    if (relationsToSave.Any())
                    {
                        foreach (var rel in relationsToSave)
                        {
                            rel.ID_Parent_Object = request.IdClassToRepairObjects;
                        }

                        var saveResult = await elasticAgent.SaveRelations(relationsToSave);

                        result.ResultState = saveResult;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the Left-Right relations!";
                        }
                        result.Result.CountRelationsChanged += relationsToSave.Count;
                    }

                    var searchRightLeftRelations = objectsToCheck.Select(obj => new clsObjectRel
                    {
                        ID_Other = obj.GUID
                    }).ToList();

                    relationsToCheckResult = await elasticAgent.GetRelationItems(searchRightLeftRelations, doIds: true);
                    result.ResultState = relationsToCheckResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Right-Left relations of the objects!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    result.Result.CountRelations += relationsToCheckResult.Result.Count;
                    relationsToSave = relationsToCheckResult.Result.Where(rel => rel.ID_Parent_Other != request.IdClassToRepairObjects).ToList();

                    if (relationsToSave.Any())
                    {
                        foreach (var rel in relationsToSave)
                        {
                            rel.ID_Parent_Other = request.IdClassToRepairObjects;
                        }

                        var saveResult = await elasticAgent.SaveRelations(relationsToSave);

                        result.ResultState = saveResult;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the Right-Left relations!";
                        }
                        result.Result.CountRelationsChanged += relationsToSave.Count;
                    }

                    count -= pageSize;

                    request.MessageOutput?.OutputInfo($"Checked {objectsToCheck.Count} objects, {count} objects left. Changed {result.Result.CountAttributesChanged} Attributes and {result.Result.CountRelationsChanged} Relations.");

                    skip += pageSize;
                }

                



                return result;
            });

            return taskResult;
        }

        public MaintenenceController(Globals globals) : base(globals)
        {
        }
    }
}
