﻿using MaintainenceModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Services
{
    public class StatisticsServiceAgentElastic : ElasticBaseAgent
    {

        public async Task<ResultItem<OntologyStatisticRaw>> GenerateOntologyStatistics(IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<OntologyStatisticRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new OntologyStatisticRaw()
                };

                messageOutput?.OutputInfo("Generate statistic-object");
                result.Result.Statistic = new OntologyClasses.BaseClasses.clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                    GUID_Parent = Statistics.Config.LocalData.Class_Statistics__Ontology_Module_.GUID,
                    Type = globals.Type_Object
                };
                var dbConnector = new OntologyModDBConnector(globals);
                var relationConfig = new clsRelationConfig(globals);

                messageOutput?.OutputInfo("Get count of AttributeTypes...");
                result.ResultState = dbConnector.GetDataAttributeType(null, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error While getting the AttributeTypes!";
                    return result;
                }

                messageOutput?.OutputInfo("Have count of AttributeTypes.");

                result.Result.CountAttributeTypesItem = relationConfig.Rel_ObjectAttribute(result.Result.Statistic, Statistics.Config.LocalData.AttributeType_Count_Attribute_Types, result.ResultState.Count);

                messageOutput?.OutputInfo("Get count of ClassAttributes...");
                result.ResultState = dbConnector.GetDataClassAtts(null, null, true, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error While getting the ClassAttributes!";
                    return result;
                }

                result.Result.CountClassAttributesItem = relationConfig.Rel_ObjectAttribute(result.Result.Statistic, Statistics.Config.LocalData.AttributeType_Count_Class_Attributes, result.ResultState.Count);

                messageOutput?.OutputInfo("Have count of ClassAttributes.");

                messageOutput?.OutputInfo("Get count of ClassRelations...");
                result.ResultState = dbConnector.GetDataClassRel(null, false, false, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error While getting the regular ClassRelations!";
                    return result;
                }

                var countClassRel = result.ResultState.Count;

                result.ResultState = dbConnector.GetDataClassRel(null, false, true, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error While getting the special ClassRelations!";
                    return result;
                }

                countClassRel += result.ResultState.Count;

                result.Result.CountClassRelationsItem = relationConfig.Rel_ObjectAttribute(result.Result.Statistic, Statistics.Config.LocalData.AttributeType_Count_Class_Relations, countClassRel);

                messageOutput?.OutputInfo("Have count of ClassRelations.");

                messageOutput?.OutputInfo("Get count of Classes...");
                result.ResultState = dbConnector.GetDataClasses(null, false, null, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error While getting the special ClassRelations!";
                    return result;
                }

                result.Result.CountClassesItem = relationConfig.Rel_ObjectAttribute(result.Result.Statistic, Statistics.Config.LocalData.AttributeType_Count_Classes, result.ResultState.Count);

                messageOutput?.OutputInfo("Have count of Classes...");

                messageOutput?.OutputInfo("Get count of ObjectAttributes...");
                result.ResultState = dbConnector.GetDataObjectAtt(null, true, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error While getting the special Object-Attributes!";
                    return result;
                }

                result.Result.CountObjectAttributesItem = relationConfig.Rel_ObjectAttribute(result.Result.Statistic, Statistics.Config.LocalData.AttributeType_Count_Object_Attributes, result.ResultState.Count);

                messageOutput?.OutputInfo("Have count of ObjectAttributes.");

                messageOutput?.OutputInfo("Get count of ObjectRelations...");
                result.ResultState = dbConnector.GetDataObjectRel(null, true, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error While getting the special Object-Relations!";
                    return result;
                }

                result.Result.CountObjectRelationsItem = relationConfig.Rel_ObjectAttribute(result.Result.Statistic, Statistics.Config.LocalData.AttributeType_Count_Object_Relations, result.ResultState.Count);
                messageOutput?.OutputInfo("Have count of ObjectRelations.");

                messageOutput?.OutputInfo("Get count of Objects...");
                result.ResultState = dbConnector.GetDataObjects(null, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error While getting the special Objects!";
                    return result;
                }

                result.Result.CountObjectsItem = relationConfig.Rel_ObjectAttribute(result.Result.Statistic, Statistics.Config.LocalData.AttributeType_Count_Objects, result.ResultState.Count);

                messageOutput?.OutputInfo("Have count of Objects.");

                messageOutput?.OutputInfo("Get count of RelationTypes...");
                result.ResultState = dbConnector.GetDataRelationTypes(null, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error While getting the special Objects!";
                    return result;
                }

                result.Result.CountRelationTypesItem = relationConfig.Rel_ObjectAttribute(result.Result.Statistic, Statistics.Config.LocalData.AttributeType_Count_Relation_Types, result.ResultState.Count);

                messageOutput?.OutputInfo("Have count of RelationTypes.");

                result.Result.DateTimeStampItem = relationConfig.Rel_ObjectAttribute(result.Result.Statistic, Statistics.Config.LocalData.AttributeType_DateTimestamp, DateTime.Now);

                result.ResultState = dbConnector.SaveObjects(new List<OntologyClasses.BaseClasses.clsOntologyItem> { result.Result.Statistic });

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the statistic-object!";
                    return result;
                }

                messageOutput?.OutputInfo("Save Attributes...");
                result.ResultState = dbConnector.SaveObjAtt(new List<OntologyClasses.BaseClasses.clsObjectAtt>
                {
                    result.Result.CountAttributeTypesItem,
                    result.Result.CountClassAttributesItem,
                    result.Result.CountClassesItem,
                    result.Result.CountClassRelationsItem,
                    result.Result.CountObjectAttributesItem,
                    result.Result.CountObjectRelationsItem,
                    result.Result.CountObjectsItem,
                    result.Result.CountRelationTypesItem,
                    result.Result.DateTimeStampItem
                });

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the count-attributes!";
                    return result;
                }

                messageOutput?.OutputInfo("Saved Attributes.");

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<AxiomValidationRaw>>> GenerateValidationStatistics(clsOntologyItem statisticItem, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<List<AxiomValidationRaw>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<AxiomValidationRaw>()
                };

                var dbReaderClasses = new OntologyModDBConnector(globals);
                var dbReaderClassAttributes = new OntologyModDBConnector(globals);
                var dbReaderClassRelations = new OntologyModDBConnector(globals);
                var dbReaderObjectAtts = new OntologyModDBConnector(globals);
                var dbReaderObjects = new OntologyModDBConnector(globals);
                var dbReaderObjectRels = new OntologyModDBConnector(globals);

                var relationConfig = new clsRelationConfig(globals);
                var objectsToSave = new List<clsOntologyItem>();
                var attributesToSave = new List<clsObjectAtt>();
                var relationsToSave = new List<clsObjectRel>();

                messageOutput?.OutputInfo("Get Classes");
                result.ResultState = dbReaderClasses.GetDataClasses();
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Classes";
                    return result;
                }

                messageOutput?.OutputInfo("Have Classes.");

                messageOutput?.OutputInfo("Get Class-Attributes...");
                result.ResultState = dbReaderClassAttributes.GetDataClassAtts();
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Class-Attributes";
                    return result;
                }

                messageOutput?.OutputInfo("Have Classes-Attributes.");

                messageOutput?.OutputInfo("Get Class-Relations...");
                result.ResultState = dbReaderClassRelations.GetDataClassRel();
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Class-Relations";
                    return result;
                }

                messageOutput?.OutputInfo("Have Classes-Relations.");


                foreach (var cls in dbReaderClasses.Classes1)
                {
                    var classAttributes = dbReaderClassAttributes.ClassAtts.Where(clsAtt => clsAtt.ID_Class == cls.GUID).ToList();
                    var classRelations = dbReaderClassRelations.ClassRels.Where(clsRel => clsRel.ID_Class_Left == cls.GUID).ToList();
                    if (classAttributes.Any() || classRelations.Any())
                    {
                        var searchObjects = new List<clsOntologyItem>
                        {
                            new clsOntologyItem
                            {
                                GUID_Parent = cls.GUID
                            }
                        };

                        result.ResultState = dbReaderObjects.GetDataObjects(searchObjects);
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting the Objects.";
                            return result;
                        }

                        var objectAtts = new List<clsObjectAtt>();
                        var objectRels = new List<clsObjectRel>();
                        
                        long validatedCount = 0;
                        long attributeViolationCount = 0;
                        if (classAttributes.Any())
                        {
                            var searchObjectAtts = new List<clsObjectAtt>
                            {
                                new clsObjectAtt
                                {
                                    ID_Class = cls.GUID
                                }
                            };

                            result.ResultState = dbReaderObjectAtts.GetDataObjectAtt(searchObjectAtts);
                            if (result.ResultState.GUID == globals.LState_Error.GUID)
                            {

                            }
                            var objAttCounts = dbReaderObjectAtts.ObjAtts.GroupBy(objAtt => new { objAtt.ID_Object, objAtt.ID_AttributeType }).Select(grp => new { IdObject = grp.Key.ID_Object, IdAttributeType = grp.Key.ID_AttributeType, Count = grp.Count() });

                            validatedCount += dbReaderObjectAtts.ObjAtts.Count;
                            foreach (var validationCheckItem  in (from objAttCount in objAttCounts
                                                             join clsAtt in classAttributes on objAttCount.IdAttributeType equals clsAtt.ID_AttributeType
                                                             select new { objAttCount, clsAtt }))
                            {
                                var objAttCount = validationCheckItem.objAttCount.Count;

                                if (objAttCount < validationCheckItem.clsAtt.Min)
                                {
                                    attributeViolationCount++;
                                }
                                else if (validationCheckItem.clsAtt.Max != -1 && objAttCount > validationCheckItem.clsAtt.Max)
                                {
                                    attributeViolationCount++;
                                }
                            }

                        }

                        if (classRelations.Any())
                        {
                            var searchObjectRels = new List<clsObjectRel>
                            {
                                new clsObjectRel
                                {
                                    ID_Parent_Object = cls.GUID
                                }
                            };

                            result.ResultState = dbReaderObjectRels.GetDataObjectRel(searchObjectRels);
                            var objRels = dbReaderObjectRels.ObjectRels.GroupBy(rel => new { rel.ID_Object, rel.ID_RelationType, rel.ID_Parent_Other }).Select(grp => new { grp.Key.ID_Object, grp.Key.ID_RelationType, grp.Key.ID_Parent_Other, Count = grp.Count() }).ToList();
                            var objObjRels = (from obj in dbReaderObjects.Objects1
                                              join objRel in objRels on obj.GUID equals objRel.ID_Object into objRels1
                                              from objRel in objRels1.DefaultIfEmpty()
                                              select new { obj, objRel }).ToList();


                            foreach (var clsRel in classRelations.Where(rel => rel.Min_Forw > 0))
                            {

                            }

                            
                        }

                        var name = $"attr: {cls.Name}";
                        if (name.Length > 255)
                        {
                            name = name.Substring(0, 255);
                        }
                        var axiomValidation = new clsOntologyItem
                        {
                            GUID = globals.NewGUID,
                            Name = name,
                            GUID_Parent = Statistics.Config.LocalData.Class_Axiom_Validations.GUID,
                            Type = globals.Type_Object
                        };
                        var validationItem = new AxiomValidationRaw
                        {
                            Statistic = statisticItem,
                            AxiomValidation = axiomValidation,
                            Count = relationConfig.Rel_ObjectAttribute(axiomValidation, Statistics.Config.LocalData.AttributeType_Count, validatedCount),
                            Violations = relationConfig.Rel_ObjectAttribute(axiomValidation, Statistics.Config.LocalData.AttributeType_Violations, attributeViolationCount),
                            References = new List<clsObjectRel> { relationConfig.Rel_ObjectRelation(axiomValidation, cls, Statistics.Config.LocalData.RelationType_belonging_Resource) }
                        };
                        objectsToSave.Add(axiomValidation);
                        attributesToSave.Add(validationItem.Count);
                        attributesToSave.Add(validationItem.Violations);
                        relationsToSave.AddRange(validationItem.References);
                        result.Result.Add(validationItem);
                    }

                    



                }


                return result;
            });

            return taskResult;
        }

        public StatisticsServiceAgentElastic(Globals globals) : base(globals)
        {
            
        }
    }
}
