﻿using MaintainenceModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Services
{
    public class ServiceAgentElastic : ElasticBaseAgent
    {

        public async Task<ResultItem<DeleteUnrelatedObjectsConfig>>  GetDeleteUnrelatedObjectsConfig(DeleteUnrelatedObjectsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<DeleteUnrelatedObjectsConfig>>(() =>
           {
               var result = new ResultItem<DeleteUnrelatedObjectsConfig>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new DeleteUnrelatedObjectsConfig()
               };

               result.ResultState = Validation.ValidationController.ValidateDeleteUnrelatedObjectsRequest(request, globals);
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchRootConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = DeleteUnrelatedObjects.Config.LocalData.Class_Delete_Unrelated_Objects.GUID
                   }
               };

               var dbReaderRootConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the root-config!";
                   return result;
               }

               result.Result.RootConfig = dbReaderRootConfig.Objects1.FirstOrDefault();

               result.ResultState = Validation.ValidationController.ValidateDeleteUnrelatedObjectsConfigResult(result.Result, globals);
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchSubConfigs = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.RootConfig.GUID,
                       ID_RelationType = DeleteUnrelatedObjects.Config.LocalData.ClassRel_Delete_Unrelated_Objects_contains_Delete_Unrelated_Objects.ID_RelationType,
                       ID_Parent_Other = DeleteUnrelatedObjects.Config.LocalData.ClassRel_Delete_Unrelated_Objects_contains_Delete_Unrelated_Objects.ID_Class_Right
                   }
               };

               var dbReaderSubConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the sub-configs!";
                   return result;
               }

               result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               result.Result.Configs.Add(result.Result.RootConfig);

               var searchClasses = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = DeleteUnrelatedObjects.Config.LocalData.ClassRel_Delete_Unrelated_Objects_belonging_Class.ID_RelationType
               }).ToList();

               var dbReaderClasses = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderClasses.GetDataObjectRel(searchClasses);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the related classes for filtering!";
                   return result;
               }

               result.Result.FilterClasses = dbReaderClasses.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               result.ResultState = Validation.ValidationController.ValidateDeleteUnrelatedObjectsConfigResult(result.Result, globals, nameof(DeleteUnrelatedObjectsConfig.FilterClasses));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetClasses(DeleteUnrelatedObjectsConfig config)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               var dbReaderClasses = new OntologyModDBConnector(globals);

               var filterClasses = config.FilterClasses.GroupBy(cls => new { GUID = cls.GUID, Name = cls.Name, GUID_Parent = cls.GUID_Parent }).Select(grp => new clsOntologyItem
               {
                   GUID = grp.Key.GUID,
                   Name = grp.Key.Name,
                   GUID_Parent = grp.Key.GUID_Parent,
                   Type = globals.Type_Class
               }).ToList();

               result.ResultState = dbReaderClasses.GetDataClasses(filterClasses);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Classes!";
                   return result;
               }

               result.Result = dbReaderClasses.Classes1;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjectsOfClass(clsOntologyItem classItem)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Error.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               var searchObjects = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID_Parent = classItem.GUID
                   }
               };

               var dbReaderObjects = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderObjects.GetDataObjects(searchObjects);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = $"Error while getting the objects of class {classItem.Name}!";
                   return result;
               }

               result.Result = dbReaderObjects.Objects1;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetRelationsOfObjects(List<clsOntologyItem> objects, int pos, int range)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               var searchRelations = objects.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID
               }).ToList();

               searchRelations.AddRange(objects.Select(obj => new clsObjectRel
               {
                   ID_Other = obj.GUID
               }));

               var dbReaderRelations = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRelations.GetDataObjectRel(searchRelations);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = $"Error while getting the relations of the objects of range {pos} - {range}";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectAtt>>> GetObjectAttributesOfObjects(List<clsOntologyItem> objects, int pos, int range)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectAtt>>>(() =>
           {
               var result = new ResultItem<List<clsObjectAtt>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectAtt>()
               };

               var searchAttributes = objects.Select(obj => new clsObjectAtt
               {
                   ID_Object = obj.GUID
               }).ToList();

               var dbReaderAttributes = new OntologyModDBConnector(globals);

               if (searchAttributes.Any())
               {
                   result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = $"Error while getting the attributes of the objects of range {pos} - {range}";
                       return result;
                   }
               }

               result.Result = dbReaderAttributes.ObjAtts;

               return result;
           });

            return taskResult;
        }

        public async Task<clsOntologyItem> DeleteObjects(List<clsOntologyItem> objects)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Error.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                result = dbWriter.DelObjects(objects);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while deleting the objects!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<MapPathToApplicationByReplaceModel>> GetMapPathToApplicationByReplaceModel()
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<MapPathToApplicationByReplaceModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new MapPathToApplicationByReplaceModel()
                };

                var searchLogsToApplications = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = MaintainenceModule.MapPathToApplicationByReplaceLog.Config.LocalData.ClassRel_Replace_Log_belongs_to_Application.ID_Class_Left,
                        ID_RelationType = MaintainenceModule.MapPathToApplicationByReplaceLog.Config.LocalData.ClassRel_Replace_Log_belongs_to_Application.ID_RelationType,
                        ID_Parent_Other = MaintainenceModule.MapPathToApplicationByReplaceLog.Config.LocalData.ClassRel_Replace_Log_belongs_to_Application.ID_Class_Right
                    }
                };

                var dbReaderLogsToApplications = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderLogsToApplications.GetDataObjectRel(searchLogsToApplications);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the maps of Replace-Log to Application!";
                    return result;
                }

                result.Result.ReplaceLogsToApplications = dbReaderLogsToApplications.ObjectRels;

                var searchLogsToReplaced = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Class = MaintainenceModule.MapPathToApplicationByReplaceLog.Config.LocalData.Class_Replace_Log.GUID,
                        ID_AttributeType = MaintainenceModule.MapPathToApplicationByReplaceLog.Config.LocalData.AttributeType_Replaced.GUID,
                        Val_Bit = true
                    }
                };

                var dbReaderLogsToReplaced = new OntologyModDBConnector(globals);

                if (searchLogsToReplaced.Any())
                {
                    result.ResultState = dbReaderLogsToReplaced.GetDataObjectAtt(searchLogsToReplaced);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Paths of the Logs!";
                        return result;
                    }
                }

                result.Result.ReplaceLogsReplaced = dbReaderLogsToReplaced.ObjAtts;

                var searchLogsToPaths = result.Result.ReplaceLogsReplaced.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Object,
                    ID_AttributeType = MaintainenceModule.MapPathToApplicationByReplaceLog.Config.LocalData.ClassAtt_Replace_Log_Path.ID_AttributeType
                }).ToList();

                var dbReaderLogsToPaths = new OntologyModDBConnector(globals);

                if (searchLogsToPaths.Any())
                {
                    result.ResultState = dbReaderLogsToPaths.GetDataObjectAtt(searchLogsToPaths);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Paths of the Logs!";
                        return result;
                    }
                }

                result.Result.ReplaceLogsPaths = dbReaderLogsToPaths.ObjAtts;

                return result;
            });

            return taskResult;
        }

        public ServiceAgentElastic(Globals globals) : base(globals)
        {
        }
    }
}
