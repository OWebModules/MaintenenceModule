﻿using MaintainenceModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaintainenceModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateDeleteUnrelatedObjectsRequest(DeleteUnrelatedObjectsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateDeleteUnrelatedObjectsConfigResult(DeleteUnrelatedObjectsConfig config, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == null || propertyName == nameof(DeleteUnrelatedObjectsConfig.RootConfig))
            {
                if (config.RootConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Root-Config found!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(DeleteUnrelatedObjectsConfig.FilterClasses))
            {
                if (!config.FilterClasses.All(cls => cls.Type == globals.Type_Class))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "There are items related which are no classes!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateRelateObjectsWithObjectsByNameToAttributeRequest(RelateObjectsWithObjectsByNameToAttributeRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(request.IdClassSource))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"{nameof(request.IdClassSource)} is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdClassSource))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"{nameof(request.IdClassSource)} is no valid GUID!";
                return result;
            }

            if (string.IsNullOrEmpty(request.IdClassDest))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"{nameof(request.IdClassDest)} is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdClassDest))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"{nameof(request.IdClassDest)} is no valid GUID!";
                return result;
            }

            if (string.IsNullOrEmpty(request.IdAttributeTypeDest))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"{nameof(request.IdAttributeTypeDest)} is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdAttributeTypeDest))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"{nameof(request.IdAttributeTypeDest)} is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateRepairAttributesAndRelationsRequest(RepairAttributesAndRelationsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdClassToRepairObjects))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"{nameof(RepairAttributesAndRelationsRequest.IdClassToRepairObjects)} is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdClassToRepairObjects))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"{nameof(RepairAttributesAndRelationsRequest.IdClassToRepairObjects)} is no valid Id!";
                return result;
            }
            return result;
        }
    }
}
